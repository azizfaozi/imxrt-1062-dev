# imxrt-1062-dev

This code sould be GPL

untuk load kodenya kamu bisa gunakan [tensy loader](https://www.pjrc.com/teensy/loader_cli.html)
kalau dari sini, 
1. Build dulu codenya lalu setting parameter Project>Properties>C/C++ Builds> Setting> Build step> Edit post build > 

rubah serperti ini
```bash
arm-none-eabi-objcopy -v -O ihex "${BuildArtifactFileName}" "${BuildArtifactFileBaseName}.hex"

```
lalu build dengan mcuxpresso, load image hexnya contoh

```bash 
cd Debug
teensy_loader_cli -mmcu=imxrt1062 -w IMXRT1062_freertos_hello_dev_repo.hex
```
tekan tombol reset lalu colokan, lepas tombol resetnya dan tara. 

/*
 * Copyright (c) 2015, Freescale Semiconductor, Inc.
 * Copyright 2016-2017 NXP
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

/* FreeRTOS kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"

/* Freescale includes. */
#include "fsl_device_registers.h"
#include "fsl_debug_console.h"
#include "board.h"

#include "pin_mux.h"
#include "clock_config.h"
/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define PortBlink BOARD_USER_LED_GPIO
#define PinBlink BOARD_USER_LED_GPIO_PIN
#define Count_Delay 8000000

/**********************************
 *  Variable
 * ******************************/

/* The PIN status */
volatile bool g_pinSet = false;

/* Task priorities. */
#define hello_task_PRIORITY (configMAX_PRIORITIES - 1)
/*******************************************************************************
 * Prototypes
 ******************************************************************************/
static void hello_task(void *pvParameters);

/*!
 * @brief delay a while.
 */
void delay(void);


/*******************************************************************************
 * Code
 ******************************************************************************/
/*!
 * @brief Application entry point.
 */
int main(void)
{
	 /* Define the init structure for the output LED pin*/
	    gpio_pin_config_t led_config = {kGPIO_DigitalOutput, 0, kGPIO_NoIntmode};
    /* Init board hardware. */
    BOARD_ConfigMPU();
    BOARD_InitPins();
    BOARD_BootClockRUN();
    BOARD_InitDebugConsole();

    /* Init output LED GPIO. */
    GPIO_PinInit(PortBlink, PinBlink, &led_config);


    if (xTaskCreate(hello_task, "Hello_task", configMINIMAL_STACK_SIZE + 100, NULL, hello_task_PRIORITY, NULL) !=
        pdPASS)
    {
        PRINTF("Task creation failed!.\r\n");
        while (1)
            ;
    }
    vTaskStartScheduler();
    for (;;)
        ;
}

/*!
 * @brief Task responsible for printing of "Hello world." message.
 */
static void hello_task(void *pvParameters)
{
    for (;;)
    {
    	/*
    	 *  Argumen pertama dari SDK_DelayAtLeastUs  adalah lama waktu delay
    	 *  Kamu bisa edit suapaya bisa dipake buat delay
    	 *  Ingat yah dari spesifikasi IMX-RT-1062 bisa sampe 600 MHZ
    	 *  */
    	SDK_DelayAtLeastUs(100000, SDK_DEVICE_MAXIMUM_CPU_CLOCK_FREQUENCY);
    	#if (defined(FSL_FEATURE_IGPIO_HAS_DR_TOGGLE) && (FSL_FEATURE_IGPIO_HAS_DR_TOGGLE == 1))
    	        GPIO_PortToggle(PortBlink, 1u <<PinBlink);
    	#else
    	        if (g_pinSet)
    	        {
    	        	/*
    	        	 *  	Set pin mati (kayaknya)
    	        	 *  	*/
    	            GPIO_PinWrite(PortBlink, PinBlink, 0U);
    	            g_pinSet = false;
    	        }
    	        else
    	        {
    	        	/*
    	        	 *  Set pin hidup (kayaknya)
    	        	 * */
    	            GPIO_PinWrite(PortBlink, PinBlink, 1U);
    	            g_pinSet = true;
    	        }
    	#endif /* FSL_FEATURE_IGPIO_HAS_DR_TOGGLE */
    	/*
    	 *  	Bagian task suspen ane jadiin null supaya lopingnya jalan terus
    	 * */
       // vTaskSuspend(NULL);
    }
}
/*	code fungsi nya men
 * */
void delay(void)
{
    volatile uint32_t i = 0;
    for (i = 0; i < Count_Delay; ++i)
    {
        __asm("NOP"); /* delay */
    }
}

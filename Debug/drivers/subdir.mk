################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../drivers/fsl_clock.c \
../drivers/fsl_common.c \
../drivers/fsl_gpio.c \
../drivers/fsl_lpuart.c 

OBJS += \
./drivers/fsl_clock.o \
./drivers/fsl_common.o \
./drivers/fsl_gpio.o \
./drivers/fsl_lpuart.o 

C_DEPS += \
./drivers/fsl_clock.d \
./drivers/fsl_common.d \
./drivers/fsl_gpio.d \
./drivers/fsl_lpuart.d 


# Each subdirectory must supply rules for building sources it contributes
drivers/%.o: ../drivers/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -std=gnu99 -D__REDLIB__ -DCPU_MIMXRT1062DVL6A -DCPU_MIMXRT1062DVL6A_cm7 -DSDK_DEBUGCONSOLE=1 -DXIP_EXTERNAL_FLASH=1 -DXIP_BOOT_HEADER_ENABLE=1 -DSERIAL_PORT_TYPE_UART=1 -DFSL_RTOS_FREE_RTOS -DCR_INTEGER_PRINTF -DPRINTF_FLOAT_ENABLE=0 -D__MCUXPRESSO -D__USE_CMSIS -DDEBUG -I"/home/faoziaziz/Documents/MCUXpresso_11.1.0_3209/workspace/IMXRT1062_freertos_hello_dev_repo/board" -I"/home/faoziaziz/Documents/MCUXpresso_11.1.0_3209/workspace/IMXRT1062_freertos_hello_dev_repo/source" -I"/home/faoziaziz/Documents/MCUXpresso_11.1.0_3209/workspace/IMXRT1062_freertos_hello_dev_repo" -I"/home/faoziaziz/Documents/MCUXpresso_11.1.0_3209/workspace/IMXRT1062_freertos_hello_dev_repo/drivers" -I"/home/faoziaziz/Documents/MCUXpresso_11.1.0_3209/workspace/IMXRT1062_freertos_hello_dev_repo/device" -I"/home/faoziaziz/Documents/MCUXpresso_11.1.0_3209/workspace/IMXRT1062_freertos_hello_dev_repo/CMSIS" -I"/home/faoziaziz/Documents/MCUXpresso_11.1.0_3209/workspace/IMXRT1062_freertos_hello_dev_repo/amazon-freertos/freertos_kernel/include" -I"/home/faoziaziz/Documents/MCUXpresso_11.1.0_3209/workspace/IMXRT1062_freertos_hello_dev_repo/amazon-freertos/freertos_kernel/portable/GCC/ARM_CM4F" -I"/home/faoziaziz/Documents/MCUXpresso_11.1.0_3209/workspace/IMXRT1062_freertos_hello_dev_repo/utilities" -I"/home/faoziaziz/Documents/MCUXpresso_11.1.0_3209/workspace/IMXRT1062_freertos_hello_dev_repo/component/serial_manager" -I"/home/faoziaziz/Documents/MCUXpresso_11.1.0_3209/workspace/IMXRT1062_freertos_hello_dev_repo/component/lists" -I"/home/faoziaziz/Documents/MCUXpresso_11.1.0_3209/workspace/IMXRT1062_freertos_hello_dev_repo/component/uart" -I"/home/faoziaziz/Documents/MCUXpresso_11.1.0_3209/workspace/IMXRT1062_freertos_hello_dev_repo/xip" -O0 -fno-common -g3 -Wall -c  -ffunction-sections  -fdata-sections  -ffreestanding  -fno-builtin -fmerge-constants -fmacro-prefix-map="../$(@D)/"=. -mcpu=cortex-m7 -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


